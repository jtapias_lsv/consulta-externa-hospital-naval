from django.urls import path
from . import views


from .views import Home, PacientCreate, MedicCreate, AdminCreate, CitasListas, CancelarCita, ConfirmConsulView, \
    Query_Medic, Query_Interval, Query_Avail, AssignView, ConsultasListas, listaMedicsView, ConsulUpdateView, \
    MedicaView, AvailabilityCreateView, MissAppoView, ListaCitasView, AplazaAppoView, GetMedic, SetAvailableAppointments

app_name = 'CEHN'
urlpatterns = [
    path('medics/', Query_Medic.as_view(), name='medic'),
    path('intervals/', Query_Interval.as_view(), name='intervals'),
    path('avail/assigns/', AssignView.as_view(), name="assigns"),
    path('asignar/', AssignView.as_view(), name="assign"),
    path('avail/', Query_Avail.as_view(), name='availabilities'),
    path('', Home.as_view(), name="home"),
    path('', views.login, name="login"),
    path('admin_registered/', views.admin_registered, name="admin_registered"),
    path('medic_registered/', views.medic_registered, name="medic_registered"),
    path('patient_registered/', views.patient_registered, name="patient_registered"),
    path('registrar/paciente', PacientCreate.as_view(), name="registar_paciente"),
    path('registrar/medico', MedicCreate.as_view(), name="registrar_medico"),
    path('registrar/admin', AdminCreate.as_view(), name="registrar_admin"),
    path('citas/', CitasListas.as_view(), name='citas'),
    path('cancelar/<pk>', CancelarCita.as_view(), name='cancelar'),
    path('aplazar/<pk>', AplazaAppoView.as_view(), name='aplazar'),
    path('create/availability/',AvailabilityCreateView.as_view(), name='create_availability'),
    path('consultas', ConsultasListas.as_view() ,name='consultas'),
    path('confirm/<apo>', ConfirmConsulView.as_view() ,name='confirmconsul'),
    path('medicos/',listaMedicsView.as_view(),name='medicos'),
    path('consulta/<pk>',ConsulUpdateView.as_view(), name='actualconsul'),
    path('medicamentos/<consult>', MedicaView.as_view(), name='medicamentos'),
    path('miss/<pk>', MissAppoView.as_view(), name='citaperdida'),
    path('listacitas/<medic>',ListaCitasView.as_view(), name='listacitas')
    # path('', views.index, name="index"),
    #path('dispo/<medic>',Dispo.as_view(),name='dispo'),
    #path('citas/<pacient>', CitasListas.as_view(), name='citas'),
    #url(r'cancelar/(?P<pk>\d+)$', CancelarCita.as_view(), name='cancelar'),
    # path('dispo/<medic>',Dispo.as_view(),name='dispo'),
    # path('citas/<pacient>', CitasListas.as_view(), name='citas'),
    # url(r'cancelar/(?P<pk>\d+)$', CancelarCita.as_view(), name='cancelar'),
    # path('dispo/<medic>',Dispo.as_view(),name='dispo'),
    # path('citas/<pacient>', CitasListas.as_view(), name='citas'),
    # url(r'cancelar/(?P<pk>\d+)$', CancelarCita.as_view(), name='cancelar'),
    # path('assign/', GetMedic.as_view(), name='assigning'),
    # path('assign/assigning/', SetAvailableAppointments.as_view(), name='assigning'),
]