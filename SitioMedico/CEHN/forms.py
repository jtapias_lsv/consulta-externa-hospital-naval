#import datetime

from django.utils.timezone import datetime
from django.forms.models import inlineformset_factory
from .models import Pacient, AccountUser, Medic, Appointment, Availability, Consultation, \
    DetalleMedicamento, Interval, Office
from django import forms
from django.forms import CheckboxSelectMultiple
from django.contrib.auth.forms import UserCreationForm


from django.db import transaction


class PacientForm(forms.ModelForm):
    class Meta:
        model = Pacient

        fields = [
            'name',
            'lastname1',
            'lastname2',
            'identification',
            'age',
            'sex',
            'phone',
            'address',
        ]

        labels = {

            'name': 'Nombre',
            'lastname1': 'Apellido',
            'lastname2': 'Segundo Apellido',
            'identification': 'Identificacion',
            'age': 'Edad',
            'sex': 'Sexo',
            'phone': 'Telefono',
            'address': "Direccion"
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return soloLetras(event)',
                                           'onkeyup': 'saltar(event,"id_lastname1")', 'autofocus': 'True'}),
            'lastname1': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return soloLetras(event)',
                                                'onkeyup': 'saltar(event,"id_lastname2")'}),
            'lastname2': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return soloLetras(event)',
                                                'onkeyup': 'saltar(event,"id_identification")'}),
            'identification': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return justNumbers(event);',
                                                     'onkeyup': 'saltar(event,"id_age")'}),
            'age': forms.NumberInput(attrs={'class': 'input100', 'min': '1', 'onkeyup': 'saltar(event,"id_phone")'}),
            'phone': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return justNumbers(event);',
                                            'onkeyup': 'saltar(event,"id_address")'}),
            'address': forms.TextInput(attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_username")'}),
        }


class MedicForm(forms.ModelForm):
    def clean_identification(self):
        iden = self.cleaned_data.get('identification')
        print(iden)
        qs = Pacient.objects.filter(identification=iden)
        qs1 = Medic.objects.filter(identification=iden)
        if qs.count() > 0 and qs1.count() > 0:
            raise forms.ValidationError("Esta identificacion ya existe")
        return iden


class MedicForm(forms.ModelForm):
    class Meta:
        model = Medic

        fields = [
            'name',
            'lastname1',
            'lastname2',
            'identification',
            'age',
            'sex',
            'phone',
            'address',
        ]

        labels = {
            'name': 'Nombre',
            'lastname1': 'Apellido',
            'lastname2': 'Segundo Apellido',
            'identification': 'Identificacion',
            'age': 'Edad',
            'sex': 'Sexo',
            'phone': 'Telefono',
            'address': "Direccion"
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return soloLetras(event)',
                                           'onkeyup': 'saltar(event,"id_lastname1")', 'autofocus': 'True'}),
            'lastname1': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return soloLetras(event)',
                                                'onkeyup': 'saltar(event,"id_lastname2")'}),
            'lastname2': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return soloLetras(event)',
                                                'onkeyup': 'saltar(event,"id_identification")'}),
            'identification': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return justNumbers(event);',
                                                     'onkeyup': 'saltar(event,"id_age")'}),
            'age': forms.NumberInput(attrs={'class': 'input100', 'min': '1', 'onkeyup': 'saltar(event,"id_phone")'}),
            'phone': forms.TextInput(attrs={'class': 'input100', 'onkeypress': 'return justNumbers(event);',
                                            'onkeyup': 'saltar(event,"id_address")'}),
            'address': forms.TextInput(attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_username")'}),
        }

    def clean_identification(self):
        iden = self.cleaned_data.get('identification')
        print(iden)
        qs = Pacient.objects.filter(identification=iden)
        if qs.count() > 0:
            raise forms.ValidationError("Esta identificacion ya existe")
        return iden

class PacientSignUPForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = AccountUser
        fields = [
            'email',
            'username',
            'password1',
            'password2'
        ]
        labels = {
            'password1': 'Password',
            'password2': 'Repeat password'
        }
        widgets = {
            'username': forms.TextInput(
                attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_email")', 'autofocus': 'False'}),
            'email': forms.EmailInput(attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_password1")'}),

        }

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 != password2:
            raise forms.ValidationError("Contraseñas no coinciden")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.type = "P"
        user.save()
        return user


class MedicSignUPForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = AccountUser
        fields = [
            'email',
            'username',
            'password1',
            'password2'
        ]
        labels = {
            'password1': 'Password',
            'password2': 'Repeat password'
        }
        widgets = {
            'username': forms.TextInput(
                attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_email")', 'autofocus': 'False'}),
            'email': forms.EmailInput(attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_password1")'}),

        }

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.type = "M"
        user.save()
        return user


class AdminSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = AccountUser
        fields = [
            'email',
            'username',
            'password1',
            'password2'
        ]
        labels = {
            'password1': 'Password',
            'password2': 'Repeat password'
        }
        widgets = {
            'username': forms.TextInput(
                attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_email")', 'autofocus': 'False'}),
            'email': forms.EmailInput(attrs={'class': 'input100', 'onkeyup': 'saltar(event,"id_password1")'}),

        }

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.type = "A"
        user.save()
        return user


class AvailabilityForm(forms.ModelForm):
    '''
        this form is used to change the state of an availability to true if a pacint  cancel an appointment
    '''

    def __init__(self, *args, **kwargs):
        super(AvailabilityForm, self).__init__(*args, **kwargs)
        self.initial['state'] = True
        self.fields['state'].widget = forms.HiddenInput()

    class Meta:
        model = Availability
        fields = [
            'state',
        ]
        labels = {
            'state': 'Estado'
        }


class AppointmentForm(forms.ModelForm):
    '''
        this form is used to change the state of an appointment to canceled if a pacint  cancel an appointment
    '''

    def __init__(self, *args, **kwargs):
        super(AppointmentForm, self).__init__(*args, **kwargs)

        self.initial['appointment'] = 'CA'
        self.fields['appointment'].widget = forms.HiddenInput()

    class Meta:
        model = Appointment

        fields = [
            'appointment'
        ]
        labels = {
            'appointment': 'Estado'
        }


class AvailabilityFormFull(forms.ModelForm):
    id_inter1 = forms.ModelChoiceField(label="Intervalo", queryset=Interval.objects.all(),
                                       widget=forms.Select(attrs={'class': 'input100'}))
    id_med2 = forms.ModelChoiceField(label='Medico', queryset=Medic.objects.all(),
                                     widget=forms.Select(attrs={'class': 'input100'}))
    id_off2 = forms.ModelChoiceField(label='Consultorio', queryset= Office.objects.all(),
                                     widget=forms.Select(attrs={'class': 'input100'}))
    #date = forms.DateTimeInput()


    class Meta:
        model = Availability
        fields = ('id_med2', 'id_inter1', 'date', 'id_off2')
        labels = {
            'id_med2': 'Medico',
            'id_inter1': 'Intervalo',
            'date': 'Fecha',
            'id_off2': 'Oficina'
        }
        widgets = {
            'date': forms.DateTimeInput(attrs={'class': 'input100', 'type': 'date'})
        }

    def clean_date(self, *args, **kwargs):
        fecha = self.cleaned_data.get('date')
        if datetime.date(datetime.now()) > fecha:
            raise forms.ValidationError('Fecha erronea ingrese otra')
        return fecha


class ConfirmConsulForm(forms.ModelForm):
    '''
        form to confirm patient asisitence to a medic appointmen this is secretary or admin job
        template: 'CEHN/confir_consul.html'
        url: 'CEHN:confirmconsul'
        view: ConfirmConsulView
    '''
    paciente = forms.CharField()
    fecha = forms.Field()
    hora = forms.CharField()
    medico = forms.CharField()
    consultorio = forms.CharField()

    def __init__(self, apo=None, *args, **kwargs):
        '''
            :param medic: captura el codigo del médico pasado por url
        '''
        super(ConfirmConsulForm, self).__init__(*args, **kwargs)
        cita = Appointment.objects.filter(id_appo=apo, appointment='SP')
        self.fields['id_appo2'].initial = cita[0]
        self.fields['id_appo2'].widget = forms.HiddenInput()
        self.fields['paciente'].initial = cita[0].id_pac1
        self.fields['paciente'].widget.attrs['readonly'] = True
        self.fields['fecha'].initial = cita[0].id_aval1.date
        self.fields['fecha'].widget.attrs['readonly'] = True
        self.fields['hora'].initial = cita[0].id_aval1.id_inter1
        self.fields['hora'].widget.attrs['readonly'] = True
        self.fields['medico'].initial = cita[0].id_aval1.id_med2
        self.fields['medico'].widget.attrs['readonly'] = True
        self.fields['consultorio'].initial = cita[0].id_aval1.id_off2
        self.fields['consultorio'].widget.attrs['readonly'] = True

    class Meta:
        model = Consultation

        fields = [
            'id_appo2'
        ]
        labels={
            'id_appo2':'Cita'
        }


class SetAvailableAppointments(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = '__all__'
        labels ={
            'appointment':'Estado',
            'observation':'Observaciones',
            'id_pac1':'Id Paciente',
            'id_aval1':'Id Disponibilidad'
        }


class ListaMedicsForm(forms.ModelForm):
    '''
        form to list all medics to a secretary or admin
        template: 'CEHN/listamedicos.html'
        url: 'CEHN:medicos'
        view: listaMedicsView
    '''

    class Meta:
        model = Medic

        fields = [
            'name'
        ]


class ActualConsulForm(forms.ModelForm):
    '''
        form to update a medic consultation this is medic's job
        template: 'CEHN/actual_consul.html'
        url: 'CEHN:actualconsul'
        view: ConsulUpdateView
    '''

    class Meta:
        model = Consultation
        fields = [
            'symptom',
            'diag'
        ]
        labels = {
            'symptom': 'Sintomas',
            'diag': 'Diagnóstico'
        }
        widgets = {
            'diag': CheckboxSelectMultiple
        }


class MedicaForm(forms.ModelForm):

    def __init__(self, consult=None, *args, **kwargs):
        '''
            :param consul: catch medic consultation id giving by url
        '''
        super(MedicaForm, self).__init__(*args, **kwargs)
        self.initial['consul'] = consult
        self.fields['consul'].widget = forms.HiddenInput()

    class Meta:
        model = DetalleMedicamento

        fields = [
            'consul',
            'medi',
            'quantity',
            'dosage'

        ]
        labels = {
            'consul': 'Consulta',
            'medi': 'Medicamento',
            'quantity': 'Cantidad',
            'dosage': 'Dosis'
        }
        widgets = {
            'dosage': forms.TextInput()
        }


class MissAppoForm(forms.ModelForm):
    '''
        form to missed to an appointment if patient dosen't come to medic consultation
        template: 'CEHN/miss_appo.html'
        url: 'CEHN:'
        view: MissAppoView
    '''

    def __init__(self, *args, **kwargs):
        super(MissAppoForm, self).__init__(*args, **kwargs)

        self.initial['appointment'] = 'MI'
        self.fields['appointment'].widget = forms.HiddenInput()

    class Meta:

        model = Appointment

        fields = [
            'appointment',
        ]


class ListaCitasForm(forms.ModelForm):
    '''
        form to list all apointments for a medic
        template: 'CEHN/listacitas.html'
        url: 'CEHN:listacitas'
        view: ListaCitasView
    '''

    class Meta:

        model = Appointment

        fields = [
            'id_aval1',
            'id_pac1'
        ]