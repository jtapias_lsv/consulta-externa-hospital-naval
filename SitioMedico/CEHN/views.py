from django.http import request, HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from .decorator import medic_required, pacient_required, admin_required
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from datetime import date

from django.views.generic import CreateView, TemplateView, ListView, UpdateView, View
from .models import AccountUser, Appointment, Availability, Pacient, Medic, Interval, Consultation, DetalleMedicamento
from .forms import PacientSignUPForm, PacientForm, MedicSignUPForm, MedicForm, AdminSignUpForm, \
    AvailabilityForm, AppointmentForm, ConfirmConsulForm, ListaMedicsForm, ActualConsulForm, \
    MedicaForm, AvailabilityFormFull, SetAvailableAppointments, MissAppoForm, ListaCitasForm

# Create your views here.


def index(request):
    return render(request, 'index.html')


def login(request):
    return render(request, 'registration/login.html')


def patient_registered(request):
    return render(request, 'register/patient_registered.html')


def medic_registered(request):
    return render(request, 'register/medic_registered.html')


def admin_registered(request):
    return render(request, 'register/admin_registered.html')


class Home(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    template_name = 'home.html'


@method_decorator([admin_required], name='dispatch')
class PacientCreate(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    model = Pacient
    template_name = 'register/patient_register.html'
    form_class = PacientForm
    second_form_class = PacientSignUPForm
    success_url = reverse_lazy('CEHN:patient_registered')

    def get_context_data(self, **kwargs):
        context = super(PacientCreate, self).get_context_data(**kwargs)
        if "form" not in context:
            context['form'] = self.form_class(self.request.GET)
        if "form1" not in context:
            context['form1'] = self.second_form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form1 = self.second_form_class(request.POST)
        if form.is_valid() and form1.is_valid():
            pacient = form.save(commit=False)
            pacient.userFK = form1.save()
            pacient.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(form=form, form1=form1, message="Paciente registrado con exito!"))


@method_decorator([admin_required], name='dispatch')
class MedicCreate(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    model = Medic
    template_name = 'register/medic_register.html'
    form_class = MedicForm
    second_form_class = MedicSignUPForm
    success_url = reverse_lazy('CEHN:medic_registered')

    def get_context_data(self, **kwargs):
        context = super(MedicCreate, self).get_context_data(**kwargs)
        if "form" not in context:
            context['form'] = self.form_class(self.request.GET)
        if "form1" not in context:
            context['form1'] = self.second_form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form1 = self.second_form_class(request.POST)
        if form.is_valid() and form1.is_valid():
            medic = form.save(commit=False)
            medic.userFK = form1.save()
            medic.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form1=form1))


@method_decorator([admin_required], name='dispatch')
class AdminCreate(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    model = AccountUser
    template_name = 'register/admin_register.html'
    form_class = AdminSignUpForm
    success_url = reverse_lazy('CEHN:admin_registered')


@method_decorator([pacient_required], name='dispatch')
class CitasListas(LoginRequiredMixin, ListView):
    login_url = '/login/'
    model = Appointment
    template_name = 'CEHN/citas.html'

    def get_queryset(self):
        self.pac = get_object_or_404(Pacient, userFK_id=self.request.user.id)
        return Appointment.objects.filter(id_pac1=self.pac.id, appointment='SP') | Appointment.objects.filter(id_pac1=self.pac.id, appointment='CF')


@method_decorator([pacient_required], name='dispatch')
class CancelarCita(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    model = Appointment
    second_model = Availability
    template_name = 'CEHN/cancelarcita.html'
    form_class = AppointmentForm
    second_form_class = AvailabilityForm
    success_url = reverse_lazy('CEHN:citas')

    # in this case I use two models in a view and a template
    def get_context_data(self, **kwargs):
        context = super(CancelarCita, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        cancelacion = self.model.objects.get(id_appo=pk)
        disponibilidad = self.second_model.objects.get(id_aval=(cancelacion.id_aval1).id_aval)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=disponibilidad)
        context['id'] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        cancelacion = self.model.objects.get(id_appo=kwargs['pk'])
        disponibilidad = self.second_model.objects.get(id_aval=(cancelacion.id_aval1).id_aval)
        form = self.form_class(request.POST, instance=cancelacion)
        form2 = self.second_form_class(request.POST, instance=disponibilidad)
        if form.is_valid() and form2.is_valid():
            form.save()
            form2.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return HttpResponseRedirect(self.get_success_url())


class GetMedic(ListView):
    model = Availability
    template_name = 'assign.html'
    form_class = SetAvailableAppointments

    # form_class = 'SetAvailableAppointments'

    # def get(self, request, *args, **kwargs):
    # return render(request, 'assign.html', {'form': self.form_class})

    def post(self, request, *args, **kwargs):
        a = request.POST['date']
        c = request.POST['id_med2']

        print(a, c)
        return redirect('assigning/', *args, **kwargs)


class SetAvailableAppointments(CreateView):
    model = Appointment
    template_name = 'CEHN/cancelarcita.html'
    form_class = SetAvailableAppointments

    def prub(self):
        return HttpResponse('Hola')



@method_decorator([pacient_required], name='dispatch')
class Query_Medic(LoginRequiredMixin, ListView):
    model = Medic
    template_name = 'Assign/assign.html'


@method_decorator([admin_required], name='dispatch')
class Query_Interval(LoginRequiredMixin, ListView):
    model = Interval
    template_name = 'Assign/hours.html'


@method_decorator([pacient_required], name='dispatch')
class Query_Avail(LoginRequiredMixin, ListView):
    model = Availability
    template_name = 'Assign/avail.html'

    def post(self, *args, **kwargs):
        print(self.request)
        queryset = Availability.objects.filter(id_med2=self.request.POST['id_Medic'], date=self.request.POST['date'],
                                               state=True)
        if (queryset):
            return render(self.request, 'Assign/avail.html',
                          {'qsea': queryset, 'id_Medic': queryset.first().id_med2, 'date': self.request.POST['date'], \
                           'id_off': queryset.first().id_off2})
        else:
            return render(self.request, 'Assign/avail.html', {'no': 'no hay'})


@method_decorator([admin_required], name='dispatch')
class AvailabilityCreateView(LoginRequiredMixin, View):
    template_name = 'admin/create_availability.html'

    def get(self, request, *args, **kwargs):
        form = AvailabilityFormFull()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        context = {}
        medic = request.POST['id_med2']
        office = request.POST['id_off2']
        intervalo = request.POST['id_inter1']
        date = request.POST['date']
        form = AvailabilityFormFull()
        if Availability.objects.filter(id_med2=medic, id_off2=office, id_inter1=intervalo, date=date):
            context['message'] = "Disponibilidad repetida"
            context['form'] = form
        elif Availability.objects.filter(id_off2=office, id_inter1=intervalo, date=date):
            context['message'] = "Oficina ocupada"
            context['form'] = form
        else:
            form = AvailabilityFormFull(request.POST)
            if form.is_valid():
                form.save()
                form = AvailabilityFormFull()
                context['message'] = "Disponibilidad creada con exito"
            context['form'] = form
        return render(request, self.template_name, context)


@method_decorator([pacient_required], name='dispatch')
class AssignView(LoginRequiredMixin, View):
    template_name = 'Assign/created_appointment.html'

    def post(self, request, *args, **kwargs):
        context = {}
        medic = request.POST['medico']
        office = request.POST['oficina']
        intervalo = request.POST['intervalo']
        date = request.POST['fecha']
        aval_id = get_object_or_404(Availability, id_med2=medic, id_off2=office, id_inter1=intervalo, date=date,
                                    state=True)
        pac = get_object_or_404(Pacient, userFK_id=self.request.user.id)
        cita = Appointment.objects.create(appointment='SP', observation='', id_aval1=aval_id, id_pac1=pac)
        context['message'] = "SU CITA HA SIDO CREADA CON ÉXITO"
        context['cita'] = cita

        return render(request, self.template_name, context)


@method_decorator([medic_required], name='dispatch')
class ConsultasListas(LoginRequiredMixin, ListView):
    '''
        class to list medic consultations to a specific medic
        url: 'CEHN:consultas'
    '''
    model = Consultation
    template_name = 'CEHN/consultas.html'

    def get_queryset(self):
        self.medic = get_object_or_404(Medic, userFK_id=self.request.user.id)
        return Consultation.objects.filter(id_appo2__id_aval1__id_med2=self.medic.id_Medic, id_appo2__appointment='CF')


@method_decorator([admin_required], name='dispatch')
class listaMedicsView(LoginRequiredMixin, ListView):
    '''
        class to list all medic to a secretary or admin for see all medic's appointsments
        url: 'CEHN:medicos'
    '''
    models = Medic
    template_name = 'CEHN/listamedicos.html'
    form_class = ListaMedicsForm

    def get_queryset(self):
        return Medic.objects.all()


@method_decorator([admin_required], name='dispatch')
class ConfirmConsulView(CreateView):
    '''
        class to confirm that paient asist to an appointment
        url: 'CEHN:confirmconsul'
    '''
    model = Consultation
    template_name = 'CEHN/confir_consul.html'
    form_class = ConfirmConsulForm
    success_url = reverse_lazy('CEHN:medicos')

    def get_form_kwargs(self):
        '''
            overwriting method for catch url parameters
            :return: all url parameters in a dictionary
        '''
        kwargs = super(ConfirmConsulView, self).get_form_kwargs()
        # update the kwargs for the form init method with yours
        kwargs.update(self.kwargs)  # self.kwargs contains all url conf params
        return kwargs



@method_decorator([medic_required], name='dispatch')
class ConsulUpdateView(LoginRequiredMixin,UpdateView):
    '''
        class to update a medic consultation it means whe doctors do their job
        url: 'CEHN:actualconsul'
    '''

    model = Consultation
    template_name = 'CEHN/actual_consul.html'
    form_class = ActualConsulForm
    success_url = reverse_lazy('CEHN:consultas')

    # returning all medicines given to the medic consultation's patient
    def get_context_data(self, **kwargs):
        co = self.kwargs['pk']
        kwargs['medicamentos'] = DetalleMedicamento.objects.filter(consul=co)
        return super(ConsulUpdateView, self).get_context_data(**kwargs)



@method_decorator([medic_required()], name='dispatch')
class MedicaView(LoginRequiredMixin, CreateView):
    '''
        class to input medicines giving to the consultation's patient
        url: 'CEHN:medicamentos'
    '''

    model = DetalleMedicamento
    template_name = 'CEHN/medicamentos.html'
    form_class = MedicaForm
    success_url = None

    def get_form_kwargs(self):
        ''''
            overwriting method for catch url parameters
            :return: all url parameters in a dictionary
        '''
        kwargs = super(MedicaView, self).get_form_kwargs()
        # update the kwargs for the form init method with yours
        kwargs.update(self.kwargs)  # self.kwargs contains all url conf params
        return kwargs

    # setting the url to return after saving record
    def get_success_url(self):
        con = self.kwargs['consult']
        return reverse_lazy('CEHN:actualconsul', kwargs={'pk': con})


@method_decorator([admin_required()], name='dispatch')
class ListaCitasView(LoginRequiredMixin,ListView):
    '''
        class to list appointmens for a medic
        url:
    '''
    model =Appointment
    template_name = 'CEHN/listacitas.html'
    form_class = ListaCitasForm
    success_url = reverse_lazy('CEHN:medicos')

    def get_queryset(self):
        return Appointment.objects.filter(id_aval1__id_med2__id_Medic = self.kwargs['medic'], appointment='SP',id_aval1__date = date.today())


@method_decorator([admin_required()], name='dispatch')
class MissAppoView(LoginRequiredMixin,UpdateView):
    '''
        class to missed to an appointment if patient dosen't come to medic consultation
        url:
    '''
    model = Appointment
    template_name = 'CEHN/miss_appo.html'
    form_class = MissAppoForm
    success_url = reverse_lazy('CEHN:medicos')


@method_decorator([pacient_required()], name='dispatch')
class AplazaAppoView(LoginRequiredMixin, UpdateView):

    model = Appointment
    second_model = Availability
    template_name = 'CEHN/aplazacita.html'
    form_class = AppointmentForm
    second_form_class = AvailabilityForm
    success_url = reverse_lazy('CEHN:medic')

    # in this case I use two models in a view and a template
    def get_context_data(self, **kwargs):
        context = super(AplazaAppoView, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        cancelacion = self.model.objects.get(id_appo=pk)
        disponibilidad = self.second_model.objects.get(id_aval=(cancelacion.id_aval1).id_aval)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=disponibilidad)
        context['id'] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        cancelacion = self.model.objects.get(id_appo=kwargs['pk'])
        disponibilidad = self.second_model.objects.get(id_aval=(cancelacion.id_aval1).id_aval)
        form = self.form_class(request.POST, instance=cancelacion)
        form2 = self.second_form_class(request.POST, instance=disponibilidad)
        if form.is_valid() and form2.is_valid():
            form.save()
            form2.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return HttpResponseRedirect(self.get_success_url())