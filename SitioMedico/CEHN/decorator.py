from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test


def medic_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='home'):
    '''
    Decorator for views that checks that the logged in user is a medic,
    redirects to the log-in page if necessary.
    '''
    actual_decorator = user_passes_test(
        lambda u: (u.is_active and u.type == "M") or u.is_staff,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    else:
        print('dont have acces')
    return actual_decorator



def pacient_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='home'):
    '''
    Decorator for views that checks that the logged in user is a pacient,
    redirects to the log-in page if necessary.
    '''
    actual_decorator = user_passes_test(
        lambda u: (u.is_active and u.type == "P") or u.is_staff,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator

def admin_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url='home'):
    '''
    Decorator for views that checks that the logged in user is a admin,
    redirects to the log-in page if necessary.
    '''
    actual_decorator = user_passes_test(
        lambda u: (u.is_active and u.type == "A") or u.is_staff,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator