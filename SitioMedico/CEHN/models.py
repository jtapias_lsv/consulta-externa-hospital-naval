from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save

class AccountUser(AbstractUser):
    TYPE = (
        ('M', 'Medic'),
        ('P', 'Pacient'),
        ('A', 'Admin')
    )
    type = models.CharField(max_length=1, choices=TYPE)

class Medic(models.Model):
    '''Class that contins information of a medic'''

    id_Medic = models.AutoField(primary_key=True)
    userFK = models.OneToOneField(AccountUser, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=35)
    identification = models.CharField(max_length=20, unique=True)
    lastname1 = models.CharField(max_length=35)
    lastname2 = models.CharField(max_length=35)
    age = models.IntegerField()
    phone = models.CharField(max_length=12, unique=True)
    address = models.CharField(max_length=100)
    age = models.IntegerField()
    SEX = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('U', 'Undefined'),
    )
    sex = models.CharField(max_length=1, choices=SEX)

    def __str__(self):
        return self.name + " " + self.lastname1

class Pacient(models.Model):
    '''Class that contins information of a patient'''
    id = models.AutoField(primary_key=True)
    userFK = models.OneToOneField(AccountUser, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=35)
    identification = models.CharField(max_length=20, unique=True)
    lastname1 = models.CharField(max_length=35)
    lastname2 = models.CharField(max_length=35)
    age = models.IntegerField()
    phone = models.CharField(max_length=12, unique=True)
    address = models.CharField(max_length=100)
    age = models.IntegerField()
    SEX = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('U', 'Undefined'),
    )
    sex = models.CharField(max_length=1, choices=SEX)

    def __str__(self):
        return self.name + " " + self.lastname1

class Office(models.Model):
    ''' class to set avalible rooms for a medic consultation '''

    id_off = models.AutoField(primary_key=True)
    door_number = models.CharField(max_length=20)
    address = models.TextField()

    def __str__(self):
        return self.door_number + " " + str(self.address)

class Interval(models.Model):
    '''class for intervals of time it means the period of a medic consultation'''

    id_inter = models.AutoField(primary_key=True)
    hour_start = models.TimeField()
    hour_finish = models.TimeField()

    def __str__(self):
        return str(self.hour_start) + "-" + str(self.hour_finish)

class Availability(models.Model):
    '''class to set date, hour, medic and office avalible for a medic consultation'''

    id_aval = models.AutoField(primary_key=True)
    state = models.BooleanField(default=True)
    date = models.DateField()
    id_med2 = models.ForeignKey(Medic, on_delete=models.CASCADE)
    id_inter1 = models.ForeignKey(Interval, on_delete=models.CASCADE)
    id_off2 = models.ForeignKey(Office, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.date) + " " + str(self.id_inter1) + " " + str(self.id_med2)

class Appointment(models.Model):
    '''Class that contains reservations of cmedic consultations'''

    APPOINTMENT = (
        ('SP', 'SEPARATE'),
        ('CA', 'CANCELED'),
        ('CF','CONFIRMED'),
        ('AT', 'ATENDED'),
        ('MI', 'MISSED')
    )
    id_appo = models.AutoField(primary_key=True)
    appointment = models.CharField(max_length=2, choices=APPOINTMENT, null=True, blank=True)
    observation = models.TextField()
    id_pac1 = models.ForeignKey(Pacient, on_delete=models.CASCADE)
    id_aval1 = models.ForeignKey(Availability, on_delete=models.CASCADE)

    def __str__(self):
        return "Date: " + str(self.id_aval1.date) + " Hour: " + str(self.id_aval1.id_inter1) + " Med: " + str(
            self.id_aval1.id_med2) + " Pac: " + self.id_pac1.name + " State: " + self.appointment


class Consultation(models.Model):
    """This class Consultation contains information about the meeting medics and patient"""

    id_cons = models.AutoField(primary_key=True)
    symptom = models.TextField()
    id_appo2 = models.OneToOneField(Appointment, null=True,
                                    on_delete=models.CASCADE)  # this field has to change to a foreignkey

    def __str__(self):
        return self.id_appo2.id_pac1.name


class Diagnostic(models.Model):
    """This class Diagnostic contains information about problems in health from a patient"""

    id_diag = models.AutoField(primary_key=True)
    name_diag = models.CharField(max_length=20)
    description = models.TextField()

    def __str__(self):
        return self.name_diag


class Medicine(models.Model):
    """This class Medicine contains information about medicine giving to a patient"""

    id_med = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    laboratory = models.CharField(max_length=20)
    invima = models.CharField(max_length=10, default=123)

    def __str__(self):
        return self.name

class Treatement(models.Model):
    """This class Treatement contains information about solving problems in health from a patient"""
    id_tre = models.AutoField(primary_key=True)
    name_tre = models.CharField(max_length=20)
    mm = models.ForeignKey(Medicine, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    description = models.TextField()

    def __str__(self):
        return self.name_tre


class Consultation(models.Model):
    """This class Consultation contains information about the meeting medics and patient"""

    id_cons = models.AutoField(primary_key=True)
    symptom = models.TextField()
    id_appo2 = models.OneToOneField(Appointment, null=True,
                                    on_delete=models.CASCADE)  # this field has to change to a foreignkey
    diag = models.ManyToManyField(Diagnostic, through='Cons_Diag')

    def __str__(self):
        return str(self.id_cons)+' '+self.id_appo2.id_pac1.name


class Cons_Diag(models.Model):
    """This class is resultan between Consultation and  Diagnostic"""

    id_cons1 = models.ForeignKey(Consultation, on_delete=models.CASCADE)
    id_diag1 = models.ForeignKey(Diagnostic, on_delete=models.CASCADE)

    def __str__(self):
        return self.id_cons1.id_appo2.id_pac1.name + " - " + self.id_diag1.name_diag


class DetalleMedicamento(models.Model):
    '''This class contains medicines giving to a patient in a medic consultation '''
    consul = models.ForeignKey(Consultation, on_delete=models.CASCADE)
    medi = models.ForeignKey(Medicine, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    dosage = models.TextField()

    def __str__(self):
        return self.consul.id_appo2.id_pac1.name+" - "+self.medi.name

def avail_state(sender, **kwargs):
    if kwargs['created']:
        avail = kwargs.get('instance').id_aval1
        avail.state = False
        avail.save()

# calling callback method after save (create a appointment)
post_save.connect(avail_state, Appointment)

def atten_appo(sender, **kwargs):
    if kwargs['created']:
        apo = kwargs.get('instance').id_appo2
        apo.appointment = 'CF'
        apo.save()
    else:
        apo = kwargs.get('instance').id_appo2
        apo.appointment = 'AT'
        apo.save()

# calling callback method after save (create or update a consultation)
post_save.connect(atten_appo, Consultation)