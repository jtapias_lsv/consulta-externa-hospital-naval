from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Appointment)
admin.site.register(Cons_Diag)
admin.site.register(Consultation)
admin.site.register(Diagnostic)
admin.site.register(Interval)
admin.site.register(Availability)
admin.site.register(Medic)
admin.site.register(Medicine)
admin.site.register(Office)
admin.site.register(Pacient)
admin.site.register(AccountUser)
admin.site.register(DetalleMedicamento)